package com.whphw.gravimetria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GravimetriaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GravimetriaApiApplication.class, args);
	}

}
