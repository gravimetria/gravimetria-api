# markdown
## O que é markdown?
> markdown é uma linguagem de marcação muito utilizada para documentação.

## Palavras chaves
- Cliente
- Responsavel
- Setores
- Recipiente
  - Gaiola
  - Caixas
  - Sexta de lixo cilíndrico
- Materiais
  - Tipos
    - Secos 
    - Orgânicos
    - idiferentes
  - nome
- Mensuração
  - data
## Entidades
### Cliente
- Atributos
  - Nome Fantasia
  - Razão Social
  - CNPJ
  - Endereço
  - Bairro
  - Cidade
  - CEP
### Responsavel
- Atributos
  - Nome do Responsável / Genreciador
  - Cargo
  - Contato
    - Telefone
      - Celular
      - Fixo
    - Email
  - Nome do Responsável / Elaborador
  - Contato
    - Telefone
      - Celular
      - Fixo
    - Email
### Setores
- Atributos
    - Nome
    - Recipiente
### Recipiente
- Atributos
  - Nome
  - Material
  - Altura
  - Largura
  - Cumprimento
  - Kilo
  - Volume
### Material
- Atributos
  - Tipos
    - Secos 
    - Orgânicos
    - idiferentes
### Mensuração
- Atributos
  - Data
  - Setor